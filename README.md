# Lando/WP Test

## Usage

- [Install lando](https://docs.lando.dev/basics/installation.html)
- clone this repo and `cd` into its directory
- run `lando start`
- visit one of the server URLs listed

## TODO
- figure out github token problem. can't install uconn/banner
- figure out how to bind the root of the project to the correct part of the content directory
- figure out frontend tooling