#!/bin/bash

# the lando wordpress recipe comes with wp-cli available
# TODO 
# - make the config into a loop
# - figure out if there's a better way to set the path

# create a config file with the default lando credentials for the db
wp config create --force --path=/app/www/wordpress --dbname=wordpress --dbuser=wordpress --dbpass=wordpress --dbhost=database

# for each thing, create a config setting create the raw value
# probably need to figure out how to get this into a loop
wp --path=/app/www/wordpress config set WP_DEBUG true --raw
wp --path=/app/www/wordpress config set WP_HOME "'http://' . \$_SERVER['HTTP_HOST']" --raw
wp --path=/app/www/wordpress config set WP_SITEURL "WP_HOME . '/wordpress'" --raw
wp --path=/app/www/wordpress config set WP_CONTENT_DIR "dirname(__DIR__) . '/content'" --raw
wp --path=/app/www/wordpress config set WP_CONTENT_URL "WP_HOME . '/content'" --raw